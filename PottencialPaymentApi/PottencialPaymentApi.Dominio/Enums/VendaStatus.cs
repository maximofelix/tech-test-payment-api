﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.Enums
{
    public enum VendaStatus
    {
        [Description("Aguardando pagamento")]
        AguardandoPagamento = 1,
        [Description("Pagamento aprovado")]
        PagamentoAprovado = 2,
        [Description("Enviado para a transportadora")]
        EnviadoTransportadora = 3,
        [Description("Entregue")]
        Entregue = 4,
        [Description("Cancelada")]
        Cancelada = 5
    }
}

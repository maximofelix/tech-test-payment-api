using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Entities;
using PottencialPaymentApi.Dominio.Repositories;
using PottencialPaymentApi.Dominio.CustomExceptions;
using PottencialPaymentApi.Infraestructure.Context;

namespace PottencialPaymentApi.Tests
{
    [TestClass]
    public class VendaTest
    {
        ApiDataContext context;


        [TestMethod]
        public void TestVenda_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestVenda1")
            .Options;

            context = new ApiDataContext(options);

            IVendedorRepository vendedorRepository = new VendedorRepository(context);
            var vendedor = new Vendedor("11111111111", "Adriano", "adriano@email.com", "98000-0001");
            vendedorRepository.Incluir(vendedor);
            vendedorRepository.Salvar();
            int vendedorId = vendedor.Id;

            IItemRepository itemRepository = new ItemRepository(context);
            var item = new Item { Nome = "Batata", Preco = 7 };
            itemRepository.Incluir(item);
            itemRepository.Salvar();


            IVendaRepository vendaRepository = new VendaRepository(context);
            var venda = vendaRepository.RegistrarVenda(vendedorId, new List<Item> { item });
            vendaRepository.Salvar();

            long cont = vendaRepository.Contar();

            Assert.AreEqual(cont, 1);

        }

        [TestMethod]
        [ExpectedException(typeof(VendedorNaoCadastradoException))]
        public void TestVendaVendedorNaoCadastrado_Falha()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestVenda2")
            .Options;

            context = new ApiDataContext(options);

            int vendedorId = 1;

            List<Item> itens = new List<Item>();
            IItemRepository itemRepository = new ItemRepository(context);
            
            var item = new Item { Nome = "Batata", Preco = 7 };
            itemRepository.Incluir(item);
            itens.Add(item);

            item = new Item { Nome = "Batata", Preco = 7 };
            itemRepository.Incluir(item);
            itens.Add(item);
            
            itemRepository.Salvar();


            IVendaRepository vendaRepository = new VendaRepository(context);
            var venda = vendaRepository.RegistrarVenda(vendedorId, itens);
            vendaRepository.Salvar();

            long cont = vendaRepository.Contar();

            Assert.AreEqual(cont, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(VendaSemItensException))]
        public void TestVendaSemItens_Falha()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestVenda3")
            .Options;

            context = new ApiDataContext(options);

            IVendedorRepository vendedorRepository = new VendedorRepository(context);
            var vendedor = new Vendedor("11111111111", "Adriano", "adriano@email.com", "98000-0001");
            vendedorRepository.Incluir(vendedor);
            vendedorRepository.Salvar();
            int vendedorId = vendedor.Id;

            List<Item> itens = new List<Item>();

            IVendaRepository vendaRepository = new VendaRepository(context);
            var venda = vendaRepository.RegistrarVenda(vendedorId, itens);
            vendaRepository.Salvar();

            long cont = vendaRepository.Contar();

            Assert.AreEqual(cont, 1);
        }
    }
}
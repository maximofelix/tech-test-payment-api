﻿using PottencialPaymentApi.Dominio.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.CustomExceptions
{
    public class VendaSemItensException : CustomException
    {
        public VendaSemItensException() : base("Não é possível registrar uma venda sem itens")
        {

        }
    }
}

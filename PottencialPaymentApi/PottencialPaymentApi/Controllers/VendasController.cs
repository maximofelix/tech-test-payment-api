﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Entities;
using PottencialPaymentApi.Dominio.Enums;
using PottencialPaymentApi.Dominio.Repositories;
using PottencialPaymentApi.Infraestructure.Context;
using System.Security.Principal;

namespace PottencialPaymentApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class VendasController : ControllerBase
    {
        readonly IApiDataContext _context;

        public VendasController(ApiDataContext contexto)
        {
            _context = contexto;
        }


        [HttpPost]
        public IActionResult RegistrarVenda(VendasResponse vendaResponse)
        { // Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";

            var repo = new VendaRepository(_context);

            try
            {
                Venda venda = repo.RegistrarVenda(vendaResponse.Vendedor, vendaResponse.Itens);

                return Ok(venda);
            }
            catch (CustomException ex)
            {
                return NotFound(ex.Message);
            }
            catch(Exception ex1)
            {
                return BadRequest("Falha ao registrar venda");
            }
        }

        [HttpGet]
        public IActionResult BuscarVenda(int id)
        { // Buscar venda: Busca pelo Id da venda;
            var repo = new VendaRepository(_context);
            var venda = repo.Obter(o => o.Id == id,includeProperties: "Vendedor;Items");
            if (venda == null)
                return NotFound($"Venda número {id} não localizada");

            var retorno = venda.FirstOrDefault();
            return Ok(retorno);
        }

        [HttpPut]
        public IActionResult AtualizarVenda(int id, VendaStatus StatusVenda)
        { // Atualizar venda: Permite que seja atualizado o status da venda.
            try
            {
                var repo = new VendaRepository(_context);
                var venda = repo.Obter(id);
                venda.AlterarStatus(StatusVenda);
                repo.Alterar(venda);
                repo.Salvar();
                venda = repo.Obter(o => o.Id == id, includeProperties: "Vendedor;Items").FirstOrDefault();
                return Ok(venda);
            }
            catch (CustomException cex)
            {
                return BadRequest(cex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest("Falha ao alterar status da venda");
            }
        }
    }

    public class VendasResponse
    {
        public Vendedor Vendedor { get; set; }
        public IEnumerable<Item> Itens { get; set; }
    }
}

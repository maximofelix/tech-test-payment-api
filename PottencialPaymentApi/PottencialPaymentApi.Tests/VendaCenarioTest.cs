using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Entities;
using PottencialPaymentApi.Dominio.Repositories;
using PottencialPaymentApi.Dominio.CustomExceptions;
using PottencialPaymentApi.Infraestructure.Context;

namespace PottencialPaymentApi.Tests
{
    [TestClass]
    public class VendaCenarioTest
    {
        ApiDataContext context;


        [TestMethod]
        public void TestCenarioEntregue_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestCenario1")
            .Options;

            context = new ApiDataContext(options);

            IVendedorRepository vendedorRepository = new VendedorRepository(context);
            var vendedor = new Vendedor("11111111111", "Adriano", "adriano@email.com", "98000-0001");
            vendedorRepository.Incluir(vendedor);
            vendedorRepository.Salvar();
            int vendedorId = vendedor.Id;

            List<Item> itens = new List<Item>();

            IItemRepository itemRepository = new ItemRepository(context);
            var item = new Item { Nome = "Batata", Preco = 7 };
            itemRepository.Incluir(item);
            itens.Add(item);

            item = new Item { Nome = "Cenoura", Preco = 3 };
            itemRepository.Incluir(item);
            itens.Add(item);
            itemRepository.Salvar();


            IVendaRepository vendaRepository = new VendaRepository(context);
            var venda = vendaRepository.RegistrarVenda(vendedorId, itens);
            vendaRepository.Salvar();

            long cont = vendaRepository.Contar();
            Assert.AreEqual(2, cont);

            decimal total = venda.ObterValorVenda();
            Assert.AreEqual(10, total);

            venda.AlterarStatus(Dominio.Enums.VendaStatus.PagamentoAprovado);
            venda.AlterarStatus(Dominio.Enums.VendaStatus.EnviadoTransportadora);
            venda.AlterarStatus(Dominio.Enums.VendaStatus.Entregue);
            Assert.AreEqual(Dominio.Enums.VendaStatus.Entregue, venda.Status);
        }
        [TestMethod]
        public void TestCenarioCancelado_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestCenario2")
            .Options;

            context = new ApiDataContext(options);

            IVendedorRepository vendedorRepository = new VendedorRepository(context);
            var vendedor = new Vendedor("11111111111", "Adriano", "adriano@email.com", "98000-0001");
            vendedorRepository.Incluir(vendedor);
            vendedorRepository.Salvar();
            int vendedorId = vendedor.Id;

            List<Item> itens = new List<Item>();

            IItemRepository itemRepository = new ItemRepository(context);
            var item = new Item { Nome = "Batata", Preco = 7 };
            itemRepository.Incluir(item);
            itens.Add(item);

            item = new Item { Nome = "Cenoura", Preco = 3 };
            itemRepository.Incluir(item);
            itens.Add(item);
            itemRepository.Salvar();

            IVendaRepository vendaRepository = new VendaRepository(context);
            var venda = vendaRepository.RegistrarVenda(vendedorId, itens);
            vendaRepository.Salvar();

            venda.AlterarStatus(Dominio.Enums.VendaStatus.PagamentoAprovado);
            venda.AlterarStatus(Dominio.Enums.VendaStatus.Cancelada);
            Assert.AreEqual(Dominio.Enums.VendaStatus.Cancelada, venda.Status);
        }

        [TestMethod]
        public void TestCenarioCancelado2_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestCenario1")
            .Options;

            context = new ApiDataContext(options);

            IVendedorRepository vendedorRepository = new VendedorRepository(context);
            var vendedor = new Vendedor("11111111111", "Adriano", "adriano@email.com", "98000-0001");
            vendedorRepository.Incluir(vendedor);
            vendedorRepository.Salvar();
            int vendedorId = vendedor.Id;

            List<Item> itens = new List<Item>();

            IItemRepository itemRepository = new ItemRepository(context);
            var item = new Item { Nome = "Batata", Preco = 7 };
            itemRepository.Incluir(item);
            itens.Add(item);

            item = new Item { Nome = "Cenoura", Preco = 3 };
            itemRepository.Incluir(item);
            itens.Add(item);
            itemRepository.Salvar();


            IVendaRepository vendaRepository = new VendaRepository(context);
            var venda = vendaRepository.RegistrarVenda(vendedorId, itens);
            vendaRepository.Salvar();

            venda.AlterarStatus(Dominio.Enums.VendaStatus.PagamentoAprovado);
            venda.AlterarStatus(Dominio.Enums.VendaStatus.Cancelada);
            Assert.AreEqual(Dominio.Enums.VendaStatus.Cancelada, venda.Status);
        }
    }
}
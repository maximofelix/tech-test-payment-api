﻿using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.CustomExceptions
{
    public class AlteracaoStatusInvalidaException : CustomException
    {
        public AlteracaoStatusInvalidaException() : base ("Alteração de Status inválida") 
        {

        }

        public AlteracaoStatusInvalidaException(VendaStatus antes, VendaStatus depois) : 
            base ($"Não é possível alterar o status da venda de {antes} para {depois}") 
        {

        }
    }
}

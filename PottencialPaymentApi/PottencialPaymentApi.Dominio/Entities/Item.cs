﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.Entities
{
    public class Item : EntityBase
    {
        public string Nome { get; set; }
        public decimal Preco { get; set; }
    }
}

﻿using PottencialPaymentApi.Dominio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.Contracts
{
    public interface IBaseRepository<T>:IDisposable where T : class
    {
        long Contar();
        long Contar(Expression<Func<T, bool>> filtro = null);
        void Incluir(T entity);
        void Alterar(T entity);
        void Excluir(T entity);
        T Obter(int id);
        IEnumerable<T> Obter();
        IEnumerable<T> Obter(Expression<Func<T, bool>> filtro = null, string includeProperties = "");

        int Salvar();
    }
}

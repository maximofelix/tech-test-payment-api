﻿using PottencialPaymentApi.Dominio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.Contracts
{
    public interface IVendedorRepository : IBaseRepository<Vendedor>
    {

    }
}

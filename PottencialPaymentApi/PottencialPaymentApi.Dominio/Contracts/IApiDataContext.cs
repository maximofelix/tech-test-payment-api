﻿using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Dominio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.Contracts
{
    public interface IApiDataContext : IDisposable
    {
        DbSet<Vendedor> Vendedores { get; set; }
        DbSet<Item> Itens { get; set; }
        DbSet<Venda> Vendas { get; set; }

        int SaveChanges();
    }
}

﻿using PottencialPaymentApi.Dominio.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.CustomExceptions
{
    public class VendedorNaoCadastradoException : CustomException
    {
        public VendedorNaoCadastradoException() : base ("Vendedor não cadastrado") 
        {

        }
    }
}

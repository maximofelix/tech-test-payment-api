﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.Entities
{
    public class Vendedor : EntityBase
    {
        public Vendedor()
        {

        }

        public Vendedor(string cpf, string name, string email, string telefone) : this()
        {
            CPF = cpf;
            Name = name;
            Email = email;
            Telefone = telefone;
        }
        public Vendedor(int id, string cpf, string name, string email, string telefone) : this(cpf, name, email, telefone)
        {
            Id = id;
        }
         
        public string CPF { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

    }
}

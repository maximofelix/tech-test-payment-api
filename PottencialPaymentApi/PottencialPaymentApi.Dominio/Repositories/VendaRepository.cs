﻿using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Entities;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Dominio.CustomExceptions;
using System.Linq;

namespace PottencialPaymentApi.Dominio.Repositories
{
    public class VendaRepository : IVendaRepository
    {
        readonly IApiDataContext _context;
        public VendaRepository(IApiDataContext context)
        {
            _context = context;
        }

        public long Contar()
        {
            return _context.Vendas.Count();
        }

        public long Contar(Expression<Func<Venda, bool>> filtro = null)
        {
            var vendas = Obter(filtro);
            return vendas.Count();
        }

        public void Alterar(Venda entity)
        {
            _context.Vendas.Update(entity);
        }

        public void Excluir(Venda entity)
        {
            _context.Vendas.Remove(entity);
        }

        public void Incluir(Venda entity)
        {
            _context.Vendas.Add(entity);
        }

        public Venda Obter(int id)
        {
            return _context.Vendas.Find(id);
        }

        public IEnumerable<Venda> Obter()
        {
            return _context.Vendas;
        }

        public IEnumerable<Venda> Obter(Expression<Func<Venda, bool>> filtro = null,
                                        string includeProperties = "")
        {
            var query = _context.Vendas.AsQueryable<Venda>();
            if (filtro != null)
                query = query.Where(filtro);

            if (!string.IsNullOrEmpty(includeProperties))
                foreach (var includ in includeProperties.Split(";"))
                    query = query.Include(includ);

            return query.AsEnumerable<Venda>();
        }

        public Venda RegistrarVenda(int vendedorId, IEnumerable<Item> itens)
        {
            //A inclusão de uma venda deve possuir pelo menos 1 item;
            if (itens == null || itens.Count() == 0)
                throw new VendaSemItensException();

            var vendedor = _context.Vendedores.Find(vendedorId);
            if (vendedor is null)
                throw new VendedorNaoCadastradoException();

            var venda = new Venda();
            venda.Vendedor = vendedor;
            venda.AdicionarItens(itens);

            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return venda;
        }

        public Venda RegistrarVenda(Vendedor vendedor, IEnumerable<Item> itens)
        {
            //A inclusão de uma venda deve possuir pelo menos 1 item;
            if (itens == null || itens.Count() == 0)
                throw new VendaSemItensException();

            var vendSearch = _context.Vendedores.FirstOrDefault(o => o.Id == vendedor.Id || o.CPF == vendedor.CPF);
            if (vendSearch is null)
            {
                _context.Vendedores.Add(vendedor);
                vendSearch = vendedor;
            }

            var venda = new Venda();
            venda.Vendedor = vendSearch;
            venda.AdicionarItens(itens);

            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return venda;
        }

        public int Salvar()
        {
            return _context.SaveChanges();
        }

        protected bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
                if (disposing)
                    _context.Dispose();
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


//using PottencialPaymentApi.Dominio.Contracts;
//using PottencialPaymentApi.Dominio.CustomExceptions;
//using PottencialPaymentApi.Dominio.Entities;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace PottencialPaymentApi.Dominio.Repositories
//{
//    public class VendaRepository : IVendaRepository
//    {
//        readonly IApiDataContext _context;
//        public VendaRepository(ApiDataContext context)
//        {
//            _context = context;
//        }

//        public VendaRepository()
//        {

//        }

//        public Venda RegistrarVenda(Vendedor vendedor, IEnumerable<Item> itens)
//        {
//            //A inclusão de uma venda deve possuir pelo menos 1 item;
//            if (itens == null || itens.Count() == 0)
//                throw new VendaSemItensException();

//            var venda = new Venda();
//            venda.Vendedor = vendedor;
//            venda.Items = itens;

//            _context.Vendas.Add(venda);
//            _context.SaveChanges();

//            return venda;
//        }
//    }
//}

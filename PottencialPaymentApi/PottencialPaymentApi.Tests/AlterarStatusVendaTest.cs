using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.CustomExceptions;
using PottencialPaymentApi.Dominio.Entities;
using PottencialPaymentApi.Dominio.Enums;
using PottencialPaymentApi.Dominio.Repositories;
using PottencialPaymentApi.Infraestructure.Context;

namespace PottencialPaymentApi.Tests
{
    [TestClass]
    public class AlterarStatusVendaTest
    {
        private Venda CriarVendaValida(ApiDataContext context)
        {
            IVendedorRepository vendedorRepository = new VendedorRepository(context);
            var vendedor = new Vendedor("11111111111", "Adriano", "adriano@email.com", "98000-0001");
            vendedorRepository.Incluir(vendedor);
            vendedorRepository.Salvar();
            int vendedorId = vendedor.Id;

            IItemRepository itemRepository = new ItemRepository(context);
            var item = new Item { Nome = "Batata", Preco = 7 };
            itemRepository.Incluir(item);
            itemRepository.Salvar();


            IVendaRepository vendaRepository = new VendaRepository(context);
            var venda = vendaRepository.RegistrarVenda(vendedorId, new List<Item> { item });
            vendaRepository.Salvar();

            return venda;
        }

        /*FAZER OS OUTROS TESTES - V�lidos */
        // De: Aguardando pagamento Para: Pagamento Aprovado
        // De: Aguardando pagamento Para: Cancelada
        // De: Pagamento Aprovado Para: Enviado para Transportadora
        // De: Pagamento Aprovado Para: Cancelada
        // De: Enviado para Transportador. Para: Entregue

        #region Cen�rios de Sucesso
        [TestMethod]
        public void TestStatus_Aguardando_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test1")
            .Options;

            using (var context = new ApiDataContext(options))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);
            }
        }

        [TestMethod]
        public void TestStatus_Aguardando2Aprovado_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test2")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);

                venda.AlterarStatus(VendaStatus.PagamentoAprovado);
                Assert.AreEqual(VendaStatus.PagamentoAprovado, venda.Status);
            }
        }

        [TestMethod]
        public void TestStatus_Aguardando2Cancelado_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test3")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);

                venda.AlterarStatus(VendaStatus.Cancelada);
                Assert.AreEqual(VendaStatus.Cancelada, venda.Status);
            }
        }

        [TestMethod]
        public void TestStatus_Aprovado2Transportadora_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test3")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);

                venda.AlterarStatus(VendaStatus.PagamentoAprovado);
                Assert.AreEqual(VendaStatus.PagamentoAprovado, venda.Status);

                venda.AlterarStatus(VendaStatus.EnviadoTransportadora);
                Assert.AreEqual(VendaStatus.EnviadoTransportadora, venda.Status);
            }
        }

        [TestMethod]
        public void TestStatus_Aprovado2Cancelado_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test4")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);

                venda.AlterarStatus(VendaStatus.PagamentoAprovado);
                Assert.AreEqual(VendaStatus.PagamentoAprovado, venda.Status);

                venda.AlterarStatus(VendaStatus.Cancelada);
                Assert.AreEqual(VendaStatus.Cancelada, venda.Status);
            }
        }

        [TestMethod]
        public void TestStatus_AguardandoPagamento2Entregue_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test6")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);
                venda.AlterarStatus(VendaStatus.PagamentoAprovado);
                venda.AlterarStatus(VendaStatus.EnviadoTransportadora);
                venda.AlterarStatus(VendaStatus.Entregue);
                Assert.AreEqual(VendaStatus.Entregue, venda.Status);
            }
        }
        #endregion

        [TestMethod]
        [ExpectedException(typeof(AlteracaoStatusInvalidaException))]
        public void TestStatus_Aprovado2Aguardando_Falha()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test4")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);

                venda.AlterarStatus(VendaStatus.PagamentoAprovado);
                Assert.AreEqual(VendaStatus.PagamentoAprovado, venda.Status);

                venda.AlterarStatus(VendaStatus.AguardandoPagamento);
            }
        }        

        [TestMethod]
        [ExpectedException(typeof(AlteracaoStatusInvalidaException))]
        public void TestStatus_Aguardando2Transportadora_Falha()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test5")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);
                venda.AlterarStatus(VendaStatus.EnviadoTransportadora);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(AlteracaoStatusInvalidaException))]
        public void TestStatus_Aguardando2Entregue_Falha()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test6")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);
                venda.AlterarStatus(VendaStatus.Entregue);
            }
        }
        
        [TestMethod]
        [ExpectedException(typeof(AlteracaoStatusInvalidaException))]
        public void TestStatus_PagamentoAprovado2Entregue_Falha()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "Test6")
            .Options;

            using (var context = new ApiDataContext(options))
            using (var repo = new VendaRepository(context))
            {
                var venda = CriarVendaValida(context);
                Assert.AreEqual(VendaStatus.AguardandoPagamento, venda.Status);
                venda.AlterarStatus(VendaStatus.PagamentoAprovado);
                venda.AlterarStatus(VendaStatus.Entregue);
            }
        }

    }
}
﻿using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Entities;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace PottencialPaymentApi.Dominio.Repositories
{
    public class ItemRepository : IItemRepository
    {
        readonly IApiDataContext _context;
        public ItemRepository(IApiDataContext context)
        {
            _context = context;
        }

        public long Contar()
        {
            return _context.Itens.Count();
        }

        public long Contar(Expression<Func<Item, bool>> filtro = null)
        {
            var itens = Obter(filtro);
            return itens.Count();
        }

        public void Alterar(Item entity)
        {
            _context.Itens.Update(entity);
        }

        public void Excluir(Item entity)
        {
            _context.Itens.Remove(entity);
        }

        public void Incluir(Item entity)
        {
            _context.Itens.Add(entity);
        }

        public Item Obter(int id)
        {
            return _context.Itens.Find(id);
        }

        public IEnumerable<Item> Obter()
        {
            return _context.Itens;
        }

        public IEnumerable<Item> Obter(Expression<Func<Item, bool>> filtro = null, 
                                        string includeProperties = "")
        {
            var retorno = _context.Itens;
            if (filtro != null)
                retorno.Where(filtro);
            if (!string.IsNullOrEmpty(includeProperties))
                retorno.Include(includeProperties);

            return retorno;
        }

        public int Salvar()
        {
            return _context.SaveChanges();
        }

        protected bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
                if (disposing)
                    _context.Dispose();
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

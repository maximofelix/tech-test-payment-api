﻿using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Infraestructure.Context
{
    public class ApiDataContext : DbContext, IApiDataContext
    {
        public ApiDataContext(DbContextOptions<ApiDataContext> options) : base(options)
        {

        }

        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Item> Itens { get; set; }
        public DbSet<Venda> Vendas { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.Contracts
{
    public abstract class CustomException : Exception
    {
        protected CustomException()
        {
        }

        protected CustomException(string? message) : base(message)
        {
        }

        protected CustomException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}

﻿using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Entities;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace PottencialPaymentApi.Dominio.Repositories
{
    public class VendedorRepository : IVendedorRepository
    {
        readonly IApiDataContext _context;
        public VendedorRepository(IApiDataContext context)
        {
            _context = context;
        }

        public long Contar()
        {
            return _context.Vendedores.Count();
        }

        public long Contar(Expression<Func<Vendedor, bool>> filtro = null)
        {
            var vendedores = Obter(filtro);
            return vendedores.Count();
        }

        public void Alterar(Vendedor entity)
        {
            _context.Vendedores.Update(entity);
        }

        public void Excluir(Vendedor entity)
        {
            _context.Vendedores.Remove(entity);
        }

        public void Incluir(Vendedor entity)
        {
            _context.Vendedores.Add(entity);
        }

        public Vendedor Obter(int id)
        {
            return _context.Vendedores.Find(id);
        }

        public IEnumerable<Vendedor> Obter()
        {
            return _context.Vendedores;
        }

        public IEnumerable<Vendedor> Obter(Expression<Func<Vendedor, bool>> filtro = null, 
                                        string includeProperties = "")
        {
            var retorno = _context.Vendedores;
            if (filtro != null)
                retorno.Where(filtro);
            if (!string.IsNullOrEmpty(includeProperties))
                retorno.Include(includeProperties);

            return retorno;
        }

        public int Salvar()
        {
            return _context.SaveChanges();
        }

        protected bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
                if (disposing)
                    _context.Dispose();
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Dominio.Contracts;
using PottencialPaymentApi.Dominio.Entities;
using PottencialPaymentApi.Dominio.Repositories;
using PottencialPaymentApi.Infraestructure.Context;

namespace PottencialPaymentApi.Tests
{
    [TestClass]
    public class DataContextMemoryTest
    {
        [TestInitialize]
        public void TestInicializador()
        {
            
        }

        [TestMethod]
        public void TestBaseDadosVazia_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestDatabase1")
            .Options;

            using (ApiDataContext context = new ApiDataContext(options))
            {
                var repo = new VendedorRepository(context);

                var itens = repo.Contar();
                Assert.AreEqual(0, itens);
            }
        }

        [TestMethod]
        public void TestAdicionar1Vendedores_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestDatabase2")
            .Options;

            using (ApiDataContext context = new ApiDataContext(options))
            using (var repo = new VendedorRepository(context))
            {
                repo.Incluir(new Dominio.Entities.Vendedor("11111111111", "Adriano", "adriano@email.com", "98000-0001"));
                repo.Salvar();

                var vendedores = repo.Contar();
                Assert.AreEqual(1, vendedores);
            }
        }

        [TestMethod]
        public void TestAdicionar3Vendedores_Sucesso()
        {
            var options = new DbContextOptionsBuilder<ApiDataContext>()
            .UseInMemoryDatabase(databaseName: "TestDatabase3")
            .Options;

            using (ApiDataContext context = new ApiDataContext(options))
            {
                var repo = new VendedorRepository(context);

                repo.Incluir(new Vendedor("11111111111", "Adriano", "adriano@email.com", "98000-0001"));
                repo.Incluir(new Vendedor("22222222222", "Bruna", "bruna@email.com", "98000-00002"));
                repo.Incluir(new Vendedor("33333333333", "Claudia", "claudia@email.com", "98000-0003"));
                repo.Salvar();

                var vendedores = repo.Contar();
                Assert.AreEqual(3, vendedores);
            }
        }
    }
}
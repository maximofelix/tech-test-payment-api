﻿using PottencialPaymentApi.Dominio.CustomExceptions;
using PottencialPaymentApi.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PottencialPaymentApi.Dominio.Entities
{
    public class Venda : EntityBase
    { 
        public string Pedido { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime DataVenda { get; set; }
        public virtual VendaStatus Status { get; private set; }
        public virtual IEnumerable<Item> Items { get; private set; }

        public Venda()
        {
            DataVenda = DateTime.Now;
            Status = VendaStatus.AguardandoPagamento;
            Pedido = Guid.NewGuid().ToString();
        }

        public bool AlterarStatus(VendaStatus novoStatus)
        {
            List<VendaStatus> aguardando = new List<VendaStatus> { VendaStatus.PagamentoAprovado, VendaStatus.Cancelada };
            List<VendaStatus> aprovado = new List<VendaStatus> { VendaStatus.EnviadoTransportadora, VendaStatus.Cancelada };
            List<VendaStatus> enviadoTransp = new List<VendaStatus> { VendaStatus.Entregue };

            bool sucesso = false;
            if (this.Status == VendaStatus.AguardandoPagamento && aguardando.Contains(novoStatus))
                sucesso = true;
            else if (this.Status == VendaStatus.PagamentoAprovado && aprovado.Contains(novoStatus))
                sucesso = true;
            else if (this.Status == VendaStatus.EnviadoTransportadora && enviadoTransp.Contains(novoStatus))
                sucesso = true;

            if (!sucesso)
                throw new AlteracaoStatusInvalidaException(this.Status, novoStatus);

            this.Status = novoStatus;
            return true;
        }

        public void AdicionarItens(IEnumerable<Item> itens)
        {
            this.Items = itens;
        }
        public void AdicionarItem(Item item)
        {
            this.Items.Append(item);
        }

        public decimal ObterValorVenda()
        {
            return Items.Sum(o => o.Preco);
        }
    }
}
